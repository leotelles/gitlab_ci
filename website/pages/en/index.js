/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = (doc) => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = (props) => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = (props) => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = (props) => (
      <h2 className="projectTitle">
        {props.title}
        <small>{props.tagline}</small>
      </h2>
    );

    const PromoSection = (props) => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = (props) => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        <Logo img_src={`${baseUrl}img/undraw_monitor.svg`} />
        <div className="inner">
          <ProjectTitle tagline={siteConfig.tagline} title={siteConfig.title} />
          <PromoSection>
            <Button href="#try">Try It Out</Button>
            <Button href={docUrl('doc1.html')}>Example Link</Button>
            <Button href={docUrl('doc2.html')}>Example Link 2</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const Block = (props) => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );


    
    const Dragao = () => (
      <Block background="dark">
        {[
          {
            content:
              'a criatura mais imponente da natureza',
            image: `${baseUrl}img/drag.svg`,
            imageAlign: 'left',
            title: 'Dragao',
          },
        ]}
      </Block>
    );

    const Dragao2 = () => (
      <Block background="dark">
        {[
          {
            content:
              'Um dragao chines é mais forte por causa do tamanho',
            image: `${baseUrl}img/drag2.svg`,
            imageAlign: 'left',
            title: 'Dragao chinês',
          },
        ]}
      </Block>
    );

    const Fotinhas1 = () => (
      <Block background="dark">
        {[
          {
            content:
              'oia que fofura',
            image: `${baseUrl}img/emily1.jpeg`,
            imageAlign: 'left',
            title: 'oia que fofura',
          },
        ]}
      </Block>
    );

    const Fotinhas2 = () => (
      <Block background="dark">
        {[
          {
            content:
              'mais uma para coleçao né emily',
            image: `${baseUrl}img/emily2.jpeg`,
            imageAlign: 'right',
            title: 'mais uma para coleçao né emily',
          },
        ]}
      </Block>
    );

    const Fotinhas3 = () => (
      <Block background="dark">
        {[
          {
            content:
              'olha que coisa linda',
            image: `${baseUrl}img/emily3.jpeg`,
            imageAlign: 'left',
            title: 'olha que coisa linda',
          },
        ]}
      </Block>
    );

    const Fotinhas4 = () => (
      <Block background="dark">
        {[
          {
            content:
              'Fotinha maravilhosa moça mais preguiçosa do mundo',
            image: `${baseUrl}img/emily4.jpeg`,
            imageAlign: 'right',
            title: 'Fotinha maravilhosa moça mais preguiçosa do mundo',
          },
        ]}
      </Block>
    );


    const Ryan = () => (
      <Block background="dark">
        {[
          {
            content:
              'O menino que não sai do celular',
            image: `${baseUrl}img/ryan.jpeg`,
            imageAlign: 'left',
            title: 'Pequeno ryan',
          },
        ]}
      </Block>
    );



    const Features = () => (
      <Block layout="fourColumn">
        {[
          {
            content: 'This is the content of my feature',
            image: `${baseUrl}img/undraw_react.svg`,
            imageAlign: 'top',
            title: 'Feature One',
          },
          {
            content: 'The content of my second feature',
            image: `${baseUrl}img/undraw_operating_system.svg`,
            imageAlign: 'top',
            title: 'Feature Two',
          },
        ]}
      </Block>
    );

    const Showcase = () => {
      if ((siteConfig.users || []).length === 0) {
        return null;
      }

      const showcase = siteConfig.users
        .filter((user) => user.pinned)
        .map((user) => (
          <a href={user.infoLink} key={user.infoLink}>
            <img src={user.image} alt={user.caption} title={user.caption} />
          </a>
        ));

      const pageUrl = (page) =>
        baseUrl + (language ? `${language}/` : '') + page;

      return (
        <div className="productShowcaseSection paddingBottom">
          <h2>Who is Using This?</h2>
          <p>This project is used by all these people</p>
          <div className="logos">{showcase}</div>
          <div className="more-users">
            <a className="button" href={pageUrl('users.html')}>
              More {siteConfig.title} Users
            </a>
          </div>
        </div>
      );
    };

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Features />
          <Dragao2 />
          <Dragao />
          <Ryan />
          {/* <Fotinhas1 /> */}
          {/* <Fotinhas4 />
          <Fotinhas2 />
          <Fotinhas1 />
          <Fotinhas3 /> */}
          

        </div>
      </div>
    );
  }
}

module.exports = Index;
